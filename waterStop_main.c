#include <unistd.h>

/* Driver Header files */
#include <ti/drivers/PIN.h>
#include <ti/drivers/PWM.h>
#include <ti/drivers/Power.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/drivers/timer/GPTimerCC26XX.h>
#include <ti/drivers/ADCBuf.h>
#include <xdc/runtime/Error.h>
#include <ti/sysbios/knl/Queue.h>

#include <ti/devices/DeviceFamily.h>
#include DeviceFamily_constructPath(inc/hw_memmap.h)
#include DeviceFamily_constructPath(inc/hw_cpu_itm.h)
#include DeviceFamily_constructPath(inc/hw_cpu_scs.h)
#include DeviceFamily_constructPath(inc/hw_cpu_tpiu.h)

#include <xdc/runtime/IHeap.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Memory.h>
#include <xdc/runtime/Error.h>

//#include "uDMA_UART/uDMA_UART_CC1310.h"
/* Example/Board Header files */
#include "Board/Board.h"
#include "Controls/button.h"
#include "Packets/uartPacket.h"
#include "Packets/CRC16.h"
#include "uDMA_UART/uDMA_UART_CC1310.h"

extern void rf_receiver_setup(void);

static void swithOff(mState st);

ADCBuf_Handle adcBuf; ADCBuf_Params adcBufParams; ADCBuf_Conversion continuousConversion;

const unsigned ITM_BASE_ADDRESS = 0xE0000000; const unsigned ITM_NUM_PORTS = 32; const unsigned NUM_TRIALS = 2;

/* Pin driver handles */
PIN_Handle buttonPinHandle; PIN_Handle ledPinHandle;

/* Global memory storage for a PIN_Config table */
static PIN_State buttonPinState; static PIN_State ledPinState;

UART_Handle uartHandle; UART_Params params; bool isClearQueue;

volatile uint8_t mComm; volatile uint8_t mComm_prev;

uint8_t mStatus=0, rStatus = 0, bStatus=0;

volatile uint32_t comm; volatile uint32_t comm_prev; volatile uint8_t comCounter;

#define ADCBUFFERSIZE    (0x02)
        PWM_Handle pwm1 = NULL; PWM_Handle pwm2 = NULL; uint16_t allADCBuff[5][2];
        /*
         * Initial LED pin configuration table
         *   - LEDs Board_PIN_LED0 is on.
         *   - LEDs Board_PIN_LED1 is off.
         */
        PIN_Config ledPinTable[] = { Board_PIN_LED0 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,
Board_PIN_LED1 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
4 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
5 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
12 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
//23 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,
// 24 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH  | PIN_PUSHPULL | PIN_DRVSTR_MAX,
PIN_TERMINATE};

/*
 * Application button pin configuration table:
 *   - Buttons interrupts are configured to trigger on falling edge.
 */
PIN_Config buttonPinTable[] = {
CC1310_LAUNCHXL_PIN_BTN1 | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_DIS,
CC1310_LAUNCHXL_PIN_BTN2 | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_DIS,
CC1310_LAUNCHXL_DIO15 | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_DIS,
CC1310_LAUNCHXL_DIO1 | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_DIS,
CC1310_LAUNCHXL_DIO21 | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_DIS,
PIN_TERMINATE };

PIN_Config ButtonTableWakeUp[] = {
        CC1310_LAUNCHXL_PIN_BTN1 | PIN_INPUT_EN | PIN_PULLUP | PINCC26XX_WAKEUP_NEGEDGE,
        CC1310_LAUNCHXL_PIN_BTN2 | PIN_INPUT_EN | PIN_PULLUP | PINCC26XX_WAKEUP_NEGEDGE,
PIN_TERMINATE };

Semaphore_Struct semStruct;
Semaphore_Handle semHandle;
bool isPwmRun;
uint16_t motorTimeOut;
uint16_t duty;
uint16_t sampleBufferOne[ADCBUFFERSIZE];
uint16_t sampleBufferTwo[ADCBUFFERSIZE];
uint32_t microVoltBuffer[ADCBUFFERSIZE];

uint16_t currentData;
uint16_t currentData_prev;

uint16_t feedBack;
uint16_t feedBack_prev;

bool isLed0_ON, isLed1_ON;
volatile bool isNeedToSend;

uint16_t adcMin = 4095;
uint16_t adcMax = 0;
/*
 *  ======== buttonCallbackFxn ========
 *  Pin interrupt Callback function board buttons configured in the pinTable.
 *  If Board_PIN_LED3 and Board_PIN_LED4 are defined, then we'll add them to the PIN
 *  callback function.
 */
void buttonCallbackFxn(PIN_Handle handle, PIN_Id pinId) {
    //uint32_t currVal = 0;

    /* Debounce logic, only toggle if the button is still pushed (low) */
    CPUdelay(8000 * 50);
    if(!PIN_getInputValue(pinId)){
        /* Toggle LED based on the button pressed */
        switch (pinId){
        case Board_PIN_BUTTON0:
            //currVal = PIN_getOutputValue(Board_PIN_LED0);
            //PIN_setOutputValue(ledPinHandle, Board_PIN_LED0, !currVal);
            break;

        case Board_PIN_BUTTON1:
            //currVal = PIN_getOutputValue(Board_PIN_LED1);
            //PIN_setOutputValue(ledPinHandle, Board_PIN_LED1, !currVal);
            if(isPwmRun){
                //PWM_stop(pwm1);
                //PWM_start(pwm2);
                PWM_start(pwm1);
                PIN_setOutputValue(ledPinHandle, 12, 1);
                isPwmRun = true;
                //ITM_put_string((ITM_port_t)ITM_BASE_ADDRESS, "PWM is OFF\n\r");
            }else{
                //PWM_stop(pwm2);
                PIN_setOutputValue(ledPinHandle, 12, 0);
                PWM_start(pwm1);
                isPwmRun = true;
                //ITM_put_string((ITM_port_t)ITM_BASE_ADDRESS, "PWM is ON\n\r");
            }
            break;

        default:
            /* Do nothing */
            break;
        }
    }
}

/*
 *  ======== mainThread ========
 */
uint8_t dataOut[32];
uint8_t dataIn[16];
//uartPacOutData uOutData;
void uartDataCallback(UART_Handle handle, void *buf, size_t count) {
    //isClearQueue = true;
}

void uartDataReadCallback(UART_Handle handle, void *buf, size_t count) {
    uDMA_UARTCC1310_HWAttrs const   *hwAttrs;
    //uint8_t *inBuff = (uint8_t*) buf;

    if((dataIn[0] == 0x50) && (dataIn[1] == 0x43)){
        mComm = dataIn[2];
    }else{
        hwAttrs = handle->hwAttrs;
        while (((int32_t)UARTCharGetNonBlocking(hwAttrs->baseAddr)) != -1);
    }
    uDMA_UARTCC1310_read(handle, dataIn, 16);
}

GPTimerCC26XX_Handle msTimer;
GPTimerCC26XX_Handle cTimer;
GPTimerCC26XX_Handle pwmTimer;
PIN_Handle timerPinHandle;

uint32_t timerBuf[256];
uint32_t timerBufIndex;

uint32_t tPwmIndex;
uint32_t tPauseIndex;

bool isPwmStop;
bool isPwmRun;
bool isPwmRun_prev;

bool isPauseStop;
bool isPauseRun;

uint8_t buffOutIR[16] = { 10, 25, 16, 75, 40, 42, 32, 32, 85, 44, 22, 2, 1, 5, 6 };
uint8_t buffOutIrInsex;
mState motorState;

void timerCallback(GPTimerCC26XX_Handle handle, GPTimerCC26XX_IntMask interruptMask) {
    GPTimerCC26XX_HWAttrs const *hwAttrs = handle->hwAttrs;
    uint32_t ui32Base = hwAttrs->baseAddr;

    if(ui32Base == GPT0_BASE){
        if(!isPwmStop){
            if(++tPwmIndex == buffOutIR[buffOutIrInsex]){
                tPwmIndex = 0;
                isPwmStop = true;

                // buffOutIrInsex+=1;
            }
        }
    }

    if(ui32Base == GPT1_BASE){
        if(!isPauseStop){
            if(++tPauseIndex == buffOutIR[buffOutIrInsex]){
                tPauseIndex = 0;
                isPauseStop = true;
                //buffOutIrInsex+=1;
            }
        }
    }
}

void setMstatus(mState st) {
    motorState = st;
}

static void OpenValve(void) {
    if((motorState != open) && (motorState != mUnknown)){
        PIN_setOutputValue(ledPinHandle, Board_PIN_LED1, 1);
        motorState = tryingToOpen;
        PIN_setOutputValue(ledPinHandle, 12, 1);
        duty = 0;
        PWM_start(pwm1);
        motorTimeOut = 0;
        isPwmRun = true;
    }
}

static void CloseValve(void) {
    if((motorState != close) && (motorState != mUnknown)){
        PIN_setOutputValue(ledPinHandle, Board_PIN_LED1, 1);
        motorState = tryingToClose;
        PIN_setOutputValue(ledPinHandle, 12, 0);
        duty = 1000;
        PWM_start(pwm1);
        motorTimeOut = 0;
        isPwmRun = true;
    }
}

void stopMotor() {
    if((motorState != close) && (motorState != open)){
        swithOff(middle);
    }else{
        swithOff(off);
    }
}

static void swithOff(mState st) {
    PWM_stop(pwm1);
    PIN_setOutputValue(ledPinHandle, 12, 0);
    isPwmRun = false;
    mComm = 0;
    isLed0_ON = false;
    setMstatus(st);
    PIN_setOutputValue(ledPinHandle, Board_PIN_LED1, 0);
}

static void checkCloseMotor(void) {
    uint16_t tmp = (allADCBuff[1][0] + allADCBuff[1][1]) >> 1;
    if(tmp > adcMax)
        adcMax = tmp;
    if(tmp < adcMin)
        adcMin = tmp;

    if(motorState == tryingToClose)
        swithOff(close);
    else
        swithOff(errSwitchDir_check | errorState);
}

static void checkLefrMotor(void) {
    uint16_t tmp = (allADCBuff[1][0] + allADCBuff[1][1]) >> 1;
    if(tmp > adcMax)
        adcMax = tmp;
    if(tmp < adcMin)
        adcMin = tmp;
    if(motorState == tryingToOpen)
        swithOff(open);
    else
        swithOff(errSwitchDir_check | errorState);
}

void checkMotorStatusR(void) {
    uint16_t tmp = (allADCBuff[1][0] + allADCBuff[1][1]) >> 1;
    if(tmp > adcMax)
        adcMax = tmp;
    if(tmp < adcMin)
        adcMin = tmp;

    if(motorState != mUnknown){
        if(motorState != tryingToOpen)
            swithOff(errSwitchDir_uncheck | errorState);
    }
}

void checkMotorStatusL(void) {
    uint16_t tmp = (allADCBuff[1][0] + allADCBuff[1][1]) >> 1;
    if(tmp > adcMax)
        adcMax = tmp;
    if(tmp < adcMin)
        adcMin = tmp;

    if(motorState != mUnknown){
        if(motorState != tryingToClose)
            swithOff(errSwitchDir_uncheck | errorState);
    }
}

Button allButtons[5] = { { .stateLim = 25, .pinNum = Board_PIN_BUTTON0, .actionDown = OpenValve }, { .stateLim = 25, .pinNum = Board_PIN_BUTTON1, .actionDown = CloseValve }, { .stateLim = 25, .pinNum = 15, .actionDown = stopMotor }, { .stateLim = 5, .pinNum = 1,
        .actionDown = checkLefrMotor, .actionUp = checkMotorStatusL }, { .stateLim = 5, .pinNum = 21, .actionDown = checkCloseMotor, .actionUp = checkMotorStatusR }, };

bool isMotorTime;

void msTimerCallback(GPTimerCC26XX_Handle handle, GPTimerCC26XX_IntMask interruptMask) {
    uint8_t pinState, i;
    for(i = 0; i < 5; i++){
        pinState = PIN_getInputValue(allButtons[i].pinNum);
        if(pinState != allButtons[i].pinState){
            allButtons[i].pinState = pinState;
            allButtons[i].stateCounter = 0;
        }else{
            if(++allButtons[i].stateCounter == allButtons[i].stateLim){
                allButtons[i].stateCounter = allButtons[i].stateLim - 1;
                if(allButtons[i].pinState == 0)
                    allButtons[i].state = pressed;
                if(allButtons[i].pinState == 1)
                    allButtons[i].state = unpressed;
            }
        }
    }
    isMotorTime = true;
}

PIN_Config GptPinInitTable[] = {
IOID_23 | PIN_OPENDRAIN | PIN_INPUT_DIS | PIN_NOPULL, PIN_TERMINATE }; //PIN_INPUT_EN

static PIN_State timerPinState;

static void cTimer_init(void) {
    GPTimerCC26XX_Params params;
    GPTimerCC26XX_Params_init(&params);
    params.width = GPT_CONFIG_16BIT;
    params.mode = GPT_MODE_PERIODIC;
    params.direction = GPTimerCC26XX_DIRECTION_UP;
    params.debugStallMode = GPTimerCC26XX_DEBUG_STALL_ON;
    params.matchTiming = GPTimerCC26XX_SET_MATCH_NEXT_CLOCK;
    cTimer = GPTimerCC26XX_open(Board_GPTIMER1B, &params);
    if(cTimer == NULL){
        while(1);
    }
    // Register the GPTimer interrupt
    GPTimerCC26XX_registerInterrupt(cTimer, timerCallback, GPT_INT_TIMEOUT);
    //timerPinHandle = PIN_open(&timerPinState, GptPinInitTable);
    //GPTimerCC26XX_PinMux pinMux = GPTimerCC26XX_getPinMux(cTimer);
    //PINCC26XX_setMux(timerPinHandle, PIN_ID(IOID_24), pinMux);
    //GPTimerCC26XX_setCaptureEdge(cTimer, GPT_CTL_TAEVENT_BOTH);
    GPTimerCC26XX_setLoadValue(cTimer, 1263 * 2);
    //GPTimerCC26XX_setMatchValue(cTimer, 1263);

    //HWREG(GPT1_BASE + GPT_O_TBMR) = (TIMER_CFG_B_PWM & 0xFF) | GPT_TAMR_TAPWMIE;

    //GPTimerCC26XX_start(cTimer);
}

static void msTimer_init(void) {
    GPTimerCC26XX_Params params;
    GPTimerCC26XX_Params_init(&params);
    params.width = GPT_CONFIG_16BIT;
    params.mode = GPT_MODE_PERIODIC;
    params.direction = GPTimerCC26XX_DIRECTION_UP;
    params.debugStallMode = GPTimerCC26XX_DEBUG_STALL_ON;
    params.matchTiming = GPTimerCC26XX_SET_MATCH_NEXT_CLOCK;
    msTimer = GPTimerCC26XX_open(Board_GPTIMER1B, &params);
    if(msTimer == NULL){
        while(1);
    }
    GPTimerCC26XX_registerInterrupt(msTimer, msTimerCallback, GPT_INT_TIMEOUT);
    GPTimerCC26XX_setLoadValue(msTimer, 48000);
    //GPTimerCC26XX_setMatchValue(cTimer, 1263);

    //HWREG(GPT1_BASE + GPT_O_TBMR) = (TIMER_CFG_B_PWM & 0xFF) | GPT_TAMR_TAPWMIE;

    //GPTimerCC26XX_start(cTimer);
}

PWM_Params userPwm[1];         //params
PWM_Handle Huserp[1];
extern PWM_Config PWM_config[CC1310_LAUNCHXL_PWM1];

void pwmTimerInit(void) {
    GPTimerCC26XX_Params timerParams;
    GPTimerCC26XX_Params_init(&timerParams);
    timerParams.width = GPT_CONFIG_16BIT;
    timerParams.mode = GPT_MODE_PWM;
    timerParams.direction = GPTimerCC26XX_DIRECTION_UP;
    timerParams.debugStallMode = GPTimerCC26XX_DEBUG_STALL_ON;
    timerParams.matchTiming = GPTimerCC26XX_SET_MATCH_ON_TIMEOUT;
    pwmTimer = GPTimerCC26XX_open(Board_GPTIMER0A, &timerParams);
    GPTimerCC26XX_setLoadValue(pwmTimer, 1263 * 2);
    GPTimerCC26XX_setMatchValue(pwmTimer, 1264);
    GPTimerCC26XX_registerInterrupt(pwmTimer, timerCallback, GPT_INT_CAPTURE);         //GPT_INT_TIMEOUT
    timerPinHandle = PIN_open(&timerPinState, GptPinInitTable);
    GPTimerCC26XX_PinMux pinMux = GPTimerCC26XX_getPinMux(pwmTimer);
    PINCC26XX_setMux(timerPinHandle, PIN_ID(IOID_24), pinMux);
    HWREG(GPT0_BASE + GPT_O_CTL) |= GPT_CTL_TAPWML;
    //HWREG(GPT0_BASE + GPT_O_TAMR) = (TIMER_CFG_A_PWM & 0xFF) | GPT_TAMR_TAPWMIE;
    GPTimerCC26XX_enableInterrupt(pwmTimer, GPT_INT_CAPTURE);
    GPTimerCC26XX_start(pwmTimer);
}

void delay(unsigned num_loops) {
    unsigned i;
    for(i = 0; i < num_loops; i++){
        asm (" NOP");
    }
}

static Queue_Struct uartTx;
static Queue_Handle uartTxQueue;
uint8_t channelADC;
uint8_t channelADC_prev;
void adcBufCallback(ADCBuf_Handle handle, ADCBuf_Conversion *conversion, void *completedADCBuffer, uint32_t completedChannel) {
    uint_fast16_t tmp;
    //uint_fast16_t uartTxBufferOffset = 0;
    //volatile unsigned *myport = (volatile unsigned*) ITM_BASE_ADDRESS;
    /* Adjust raw ADC values and convert them to microvolts */
    ADCBuf_adjustRawValues(handle, completedADCBuffer, ADCBUFFERSIZE, completedChannel);
    //ADCBuf_convertAdjustedToMicroVolts(handle, completedChannel, completedADCBuffer, microVoltBuffer, ADCBUFFERSIZE);

    if(++channelADC == 5){
        channelADC = 0;
        isNeedToSend = true;
    }
    //if(channelADC != channelADC_prev)
    continuousConversion.adcChannel = channelADC + 1;
    continuousConversion.sampleBuffer = (void*) &allADCBuff[channelADC][0];
    if(ADCBuf_convert(adcBuf, &continuousConversion, 1) != ADCBuf_STATUS_SUCCESS){
        while(1);
    }
    // uint32_t currVal = PIN_getOutputValue(Board_PIN_LED0);
    // PIN_setOutputValue(ledPinHandle, Board_PIN_LED0, !currVal);
}

const char msg[] = "Hello World from ITM";
unsigned trial_num, port_num = 0, port_address;

typedef struct Rec{
    Queue_Elem elem;
    Int data;
} Rec;
Queue_Handle myQ;
Rec r1, r2;
Rec *rp;
Error_Block eb;

void buttonsMngr() {
    uint8_t i;
    for(i = 0; i < 5; i++){
        if(allButtons[i].state != allButtons[i].state_prev){
            if(allButtons[i].state == pressed){
                if(allButtons[i].actionDown != 0)
                    allButtons[i].actionDown();
                bStatus |= (1 << i);
            }else{
                if(allButtons[i].actionUp != 0)
                    allButtons[i].actionUp();
                bStatus &= ~(1 << i);
            }
            allButtons[i].state_prev = allButtons[i].state;
        }
    }
}

int ledTimer = 0;
bool isOnAllLEd;
void motorMngr() {
    if((allButtons[3].state == pressed) && (allButtons[4].state == pressed)){
        swithOff(errSwitchTwo | errorState);
    }else{
        if((motorState != tryingToClose) && (motorState != tryingToOpen)){
            if(allButtons[3].state == pressed){
                motorState = open;
            }
            if(allButtons[4].state == pressed){
                motorState = close;
            }
        }
    }

    if((motorState & 0x80) == 0x80){
        PIN_setOutputValue(ledPinHandle, 4, 0);
        PIN_setOutputValue(ledPinHandle, 5, 0);
        PIN_setOutputValue(ledPinHandle, Board_PIN_LED0, 1);
    }else{
        if(motorState == close){
            PIN_setOutputValue(ledPinHandle, Board_PIN_LED0, 0);
            PIN_setOutputValue(ledPinHandle, 5, 0);
            PIN_setOutputValue(ledPinHandle, 4, 1);
        }
        if(motorState == open){
            PIN_setOutputValue(ledPinHandle, Board_PIN_LED0, 0);
            PIN_setOutputValue(ledPinHandle, 4, 0);
            PIN_setOutputValue(ledPinHandle, 5, 1);
        }
        if((motorState == mUnknown) || (motorState == off) || (motorState == middle)){
            if(++ledTimer == 500){
                if(!isOnAllLEd){
                    PIN_setOutputValue(ledPinHandle, Board_PIN_LED0, 1);
                    PIN_setOutputValue(ledPinHandle, 4, 1);
                    PIN_setOutputValue(ledPinHandle, 5, 1);
                } else{
                    PIN_setOutputValue(ledPinHandle, Board_PIN_LED0, 0);
                    PIN_setOutputValue(ledPinHandle, 4, 0);
                    PIN_setOutputValue(ledPinHandle, 5, 0);
                }
                isOnAllLEd =!isOnAllLEd;
                ledTimer = 0;
            }
        }
    }


}

void* mainThread(void *arg0) {
Error_init(&eb);
/* myQ = Queue_create(NULL, NULL);
 Queue_enqueue(myQ, &(r1.elem));
 Queue_enqueue(myQ, &(r2.elem));


 while (!Queue_empty(myQ)) {
 // Implicit cast from (Queue_Elem *) to (Rec *)
 rp = Queue_dequeue(myQ);
 }*/

CRC16_CCITT();

UART_init();
UART_Params_init(&params);
params.baudRate = 576000;
params.parityType = UART_PAR_NONE;
params.stopBits = UART_STOP_ONE;
params.dataLength = UART_LEN_8;

params.writeDataMode = UART_DATA_BINARY;
params.writeMode = UART_MODE_CALLBACK;
params.writeCallback = uartDataCallback;

params.readDataMode = UART_DATA_BINARY;
params.readMode = UART_MODE_CALLBACK;
params.readCallback = uartDataReadCallback;

uartHandle = UART_open(Board_UART0, &params);

rf_receiver_setup();

ledPinHandle = PIN_open(&ledPinState, ledPinTable);
if(!ledPinHandle){
while(1);
}

buttonPinHandle = PIN_open(&buttonPinState, buttonPinTable);
if(!buttonPinHandle){
while(1);
}

/*if(PIN_registerIntCb(buttonPinHandle, &buttonCallbackFxn) != 0){
 while(1);
 }*/

uint16_t pwmPeriod = 1000;
PWM_Params params;

PWM_init();
PWM_Params_init(&params);
params.dutyUnits = PWM_DUTY_US;
params.dutyValue = duty;
params.periodUnits = PWM_DUTY_US;
params.periodValue = pwmPeriod;
params.idleLevel = PWM_IDLE_LOW;
pwm1 = PWM_open(Board_PWM1, &params);
if(pwm1 == NULL){
while(1);
}
pwm2 = PWM_open(Board_PWM2, &params);
if(pwm1 == NULL){
while(1);
}


ADCBuf_init();

ADCBuf_Params_init(&adcBufParams);
adcBufParams.callbackFxn = adcBufCallback;
adcBufParams.recurrenceMode = ADCBuf_RECURRENCE_MODE_ONE_SHOT;        //ADCBuf_RECURRENCE_MODE_CONTINUOUS;
adcBufParams.returnMode = ADCBuf_RETURN_MODE_CALLBACK;
adcBufParams.samplingFrequency = 5300;
adcBuf = ADCBuf_open(Board_ADCBUF0, &adcBufParams);

continuousConversion.arg = NULL;
continuousConversion.adcChannel = Board_ADCBUF0CHANNEL1;
continuousConversion.sampleBuffer = &allADCBuff[0][0];
continuousConversion.sampleBufferTwo = sampleBufferTwo;
continuousConversion.samplesRequestedCount = ADCBUFFERSIZE;

if(adcBuf == NULL){
while(1);
}
if(ADCBuf_convert(adcBuf, &continuousConversion, 1) != ADCBuf_STATUS_SUCCESS){
while(1);
}

        //Semaphore_Params semParams;
/* Construct a Semaphore object to be use as a sleep lock */
        // Semaphore_Params_init(&semParams);
        //Semaphore_construct(&semStruct, 0, &semParams);
/* Obtain instance handle */
        //semHandle = Semaphore_handle(&semStruct);
        //pwmTimerInit();
        //cTimer_init();
        //PWM_start(pwm1);
        //PWM_setDuty(pwm1, duty);
        //uDMA_UARTCC1310_read(uartHandle, dataIn, 16);
        //cTimer_init();
/* Loop forever */


bool isRunAll = 0;
msTimer_init();
GPTimerCC26XX_start(msTimer);


    //uOutData.header
uDMA_UARTCC1310_read(uartHandle, dataIn, 16);
while(1){
// dataOut[0] = 0x55;
// dataOut[15] = 0x42;
//uDMA_UARTCC1310_write(uartHandle, dataOut, 16);

//Semaphore_pend(semHandle, 500);

//sleep(1);
//Power_sleep(PowerCC26XX_STANDBY);
//Power_shutdown(1, 1000);
//GPTimerCC26XX_start(cTimer);
//

/*   if(buffOutIrInsex < 15){
 if(isPwmStop){
 GPTimerCC26XX_stop(pwmTimer);
 isPwmStop = false;
 ++buffOutIrInsex;
 GPTimerCC26XX_start(cTimer);
 }
 if(isPauseStop){
 GPTimerCC26XX_stop(cTimer);
 isPauseStop = false;
 ++buffOutIrInsex;
 GPTimerCC26XX_start(pwmTimer);
 }
 }else{
 if(isRunAll && (buffOutIrInsex == 15)){
 isRunAll = false;
 buffOutIrInsex = 0;
 }
 }

 if((buffOutIrInsex == 0)&& !isRunAll){
 isRunAll = true;
 GPTimerCC26XX_stop(pwmTimer);
 GPTimerCC26XX_stop(cTimer);
 usleep(5000);
 buffOutIrInsex = 0;
 GPTimerCC26XX_start(pwmTimer);
 }*/

// Get this port address
/*port_address = ITM_BASE_ADDRESS + (4 * port_num);
 port = (ITM_port_t) port_address;
 ITM_put_string(port, "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA!!!!!\n\r");
 delay(1000000);*/
//usleep(100);

    if(isMotorTime){
    if(isPwmRun){
        if(motorState == tryingToOpen){
            if(duty > 0){
                duty -= 1;
            }
        }
        if(motorState == tryingToClose){
            if(duty < 1000){
                duty += 1;
            }
        }
        PWM_setDuty(pwm1, duty);

        if(++motorTimeOut >= 8000){
            swithOff(errTime | errorState);
            motorTimeOut = 0;
        }
    }
    isMotorTime = false;
}

if(isNeedToSend){
    if(((uDMA_UART_CC1310_Object*) (uartHandle->object))->isReadyToTransmit){
        uartPacOutData *pData = (uartPacOutData*) &dataOut[0];
        pData->header = 0x5753;
        pData->motorStatus = motorState;
        pData->butStatus = bStatus;
        pData->radioStatus = rStatus;
        pData->adc0 = (allADCBuff[0][0] + allADCBuff[0][1]) >> 1;
        pData->adc1 = (allADCBuff[1][0] + allADCBuff[1][1]) >> 1;
        pData->adc2 = (allADCBuff[2][0] + allADCBuff[2][1]) >> 1;
        pData->adc3 = (allADCBuff[3][0] + allADCBuff[3][1]) >> 1;
        pData->adc4 = (allADCBuff[4][0] + allADCBuff[4][1]) >> 1;
        pData->adcMin = adcMin;
        pData->adcMax = adcMax;
        pData->crc16 = ComputeChecksum(&dataOut[0], sizeof(uartPacOutData));
        uDMA_UARTCC1310_write(uartHandle, pData, sizeof(uartPacOutData));
        isNeedToSend = false;
    }
}
/*
 if(isClearQueue){
 uDMA_UART_CC1310_Object *object = uartHandle->object;
 Memory_free(NULL, (void*) object->pRec, sizeof(qRec));
 isClearQueue = false;
 }else{
 fromQueueToUart(uartHandle);
 }*/

if(mComm != mComm_prev){
    switch (mComm){
    case 0:
        break;
    case 1: {
        OpenValve();
    }
        break;
    case 2: {
        CloseValve();
    }
        break;
    }
    mComm_prev = mComm;
}
buttonsMngr();
motorMngr();

//PIN_remove(buttonPinHandle, CC1310_LAUNCHXL_PIN_BTN1);
//PIN_remove(buttonPinHandle, CC1310_LAUNCHXL_PIN_BTN2);
//PINCC26XX_setWakeup(ButtonTableWakeUp);
//if(PIN_registerIntCb(buttonPinHandle, &buttonCallbackFxn) != 0){
// while(1);
 //}

//GPTimerCC26XX_stop(msTimer);
//ADCBuf_convertCancel(adcBuf);


//sleep(5);
//uint32_t currVal = PIN_getOutputValue(Board_PIN_LED0);
//PIN_setOutputValue(ledPinHandle, Board_PIN_LED0, !currVal);
}
}
