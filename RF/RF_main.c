#include <ti/drivers/rf/RF.h>
#include DeviceFamily_constructPath(driverlib/RF_prop_mailbox.h)
#include "RF/RFQueue.h"
#include "smartRF_settings/smartRF_settings.h"
#include <ti/drivers/power/PowerCC26XX.h>

#include "Board/Board.h"

/* Packet RX Configuration */
#define DATA_ENTRY_HEADER_SIZE 8  /* Constant header size of a Generic Data Entry */
#define MAX_LENGTH             21 /* Max length byte the radio will accept */
#define NUM_DATA_ENTRIES       2  /* NOTE: Only two data entries supported at the moment */
#define NUM_APPENDED_BYTES     2  /* The Data Entries data field will contain:
                                   * 1 Header byte (RF433_cmdPropRx.rxConf.bIncludeHdr = 0x1)
                                   * Max 30 payload bytes
                                   * 1 status byte (RF433_cmdPropRx.rxConf.bAppendStatus = 0x1) */
/***** Variable declarations *****/
static RF_Object rfObject;
static RF_Handle rfHandle;
void callback(RF_Handle h, RF_CmdHandle ch, RF_EventMask e);

extern PIN_Handle ledPinHandle;


/* Receive dataQueue for RF Core to fill in data */
static dataQueue_t dataQueue;
static rfc_dataEntryGeneral_t *currentDataEntry;
static uint8_t packetLength;
static uint8_t *packetDataPointer;
static uint8_t packet[MAX_LENGTH + NUM_APPENDED_BYTES - 1]; /* The length byte is stored in a separate variable */

/* Buffer which contains all Data Entries for receiving data.
 * Pragmas are needed to make sure this buffer is 4 byte aligned (requirement from the RF Core) */
#if defined(__TI_COMPILER_VERSION__)
#pragma DATA_ALIGN (rxDataEntryBuffer, 4);
static uint8_t rxDataEntryBuffer[RF_QUEUE_DATA_ENTRY_BUFFER_SIZE(NUM_DATA_ENTRIES, MAX_LENGTH, NUM_APPENDED_BYTES)];
#elif defined(__IAR_SYSTEMS_ICC__)
#pragma data_alignment = 4
static uint8_t
rxDataEntryBuffer[RF433_QUEUE_DATA_ENTRY_BUFFER_SIZE(NUM_DATA_ENTRIES,
                                                  MAX_LENGTH,
                                                  NUM_APPENDED_BYTES)];
#elif defined(__GNUC__)
static uint8_t
rxDataEntryBuffer[RF433_QUEUE_DATA_ENTRY_BUFFER_SIZE(NUM_DATA_ENTRIES,
                                                  MAX_LENGTH,
                                                  NUM_APPENDED_BYTES)]
                                                  __attribute__((aligned(4)));
#else
#error This compiler is not supported.
#endif


extern volatile uint8_t mComm;
extern volatile uint8_t mComm_prev;

extern volatile uint32_t comm;
extern volatile uint32_t comm_prev;
extern volatile uint8_t comCounter;

void rf_receiver_setup() {
    RF_Params rfParams;
    RF_Params_init(&rfParams);

    if( RFQueue_defineQueue(&dataQueue,
                            rxDataEntryBuffer,
                            sizeof(rxDataEntryBuffer),
                            NUM_DATA_ENTRIES,
                            MAX_LENGTH + NUM_APPENDED_BYTES))
    {
        /* Failed to allocate space for all data entries */
        while(1);
    }

    /* Modify CMD_PROP_RX command for application needs */
    /* Set the Data Entity queue for received data */
    RF433_cmdPropRx.pQueue = &dataQueue;
    /* Discard ignored packets from Rx queue */
    RF433_cmdPropRx.rxConf.bAutoFlushIgnored = 0;
    /* Discard packets with CRC error from Rx queue */
    RF433_cmdPropRx.rxConf.bAutoFlushCrcErr = 0;
    /* Implement packet length filtering to avoid PROP_ERROR_RXBUF */
    RF433_cmdPropRx.maxPktLen = MAX_LENGTH;
    RF433_cmdPropRx.pktConf.bRepeatOk = 1;
    RF433_cmdPropRx.pktConf.bRepeatNok = 1;

    /* Request access to the radio */
#if defined(DeviceFamily_CC26X0R2)
       rfHandle = RF_open(&rfObject, &RF433_mode, (RF_RadioSetup*)&RF433_cmdPropRadioSetup, &rfParams);
   #else
    rfHandle = RF_open(&rfObject, &RF433_mode, (RF_RadioSetup*) &RF433_cmdPropRadioDivSetup, &rfParams);
#endif// DeviceFamily_CC26X0R2

    /* Set the frequency */
    RF_postCmd(rfHandle, (RF_Op*) &RF433_cmdFs, RF_PriorityNormal, NULL, 0);

    /* Enter RX mode and stay forever in RX RF_runCmd*/
    RF_EventMask terminationReason = RF_postCmd(rfHandle, (RF_Op*) &RF433_cmdPropRx, RF_PriorityNormal, &callback, RF_EventRxEntryDone);

    switch (terminationReason){
    case RF_EventLastCmdDone:
        // A stand-alone radio operation command or the last radio
        // operation command in a chain finished.
        break;
    case RF_EventCmdCancelled:
        // Command cancelled before it was started; it can be caused
        // by RF433_cancelCmd() or RF433_flushCmd().
        break;
    case RF_EventCmdAborted:
        // Abrupt command termination caused by RF433_cancelCmd() or
        // RF433_flushCmd().
        break;
    case RF_EventCmdStopped:
        // Graceful command termination caused by RF433_cancelCmd() or
        // RF433_flushCmd().
        break;
    default:
        // Uncaught error event
        while(1);
    }

    uint32_t cmdStatus = ((volatile RF_Op*) &RF433_cmdPropRx)->status;
    switch (cmdStatus){
    case PROP_DONE_OK:
        // Packet received with CRC OK
        break;
    case PROP_DONE_RXERR:
        // Packet received with CRC error
        break;
    case PROP_DONE_RXTIMEOUT:
        // Observed end trigger while in sync search
        break;
    case PROP_DONE_BREAK:
        // Observed end trigger while receiving packet when the command is
        // configured with endType set to 1
        break;
    case PROP_DONE_ENDED:
        // Received packet after having observed the end trigger; if the
        // command is configured with endType set to 0, the end trigger
        // will not terminate an ongoing reception
        break;
    case PROP_DONE_STOPPED:
        // received CMD_STOP after command started and, if sync found,
        // packet is received
        break;
    case PROP_DONE_ABORT:
        // Received CMD_ABORT after command started
        break;
    case PROP_ERROR_RXBUF:
        // No RX buffer large enough for the received data available at
        // the start of a packet
        break;
    case PROP_ERROR_RXFULL:
        // Out of RX buffer space during reception in a partial read
        break;
    case PROP_ERROR_PAR:
        // Observed illegal parameter
        break;
    case PROP_ERROR_NO_SETUP:
        // Command sent without setting up the radio in a supported
        // mode using CMD_PROP_RADIO_SETUP or CMD_RADIO_SETUP
        break;
    case PROP_ERROR_NO_FS:
        // Command sent without the synthesizer being programmed
        break;
    case PROP_ERROR_RXOVF:
        // RX overflow observed during operation
        break;
    default:
        // Uncaught error event - these could come from the
        // pool of states defined in RF433_mailbox.h
        //while(1);
        break;
    }
}

extern uint8_t rStatus;
void callback(RF_Handle h, RF_CmdHandle ch, RF_EventMask e) {
    if(e & RF_EventRxEntryDone){
        /* Toggle pin to indicate RX */
         //PIN_setOutputValue(ledPinHandle, Board_PIN_LED2, !PIN_getOutputValue(Board_PIN_LED2));
        //GPIO_toggle(Board_GPIO_LED0);
        /* Get current unhandled data entry */
        currentDataEntry = RFQueue_getDataEntry();

        /* Handle the packet data, located at &currentDataEntry->data:
         * - Length is the first byte with the current configuration
         * - Data starts from the second byte */
        //packetLength      = *(uint8_t*)(&currentDataEntry->data);
        // packetDataPointer = (uint8_t*)(&currentDataEntry->data + 1);
        uint32_t marker = *(uint32_t*) &rxDataEntryBuffer[10];
        if(marker == 0x9F9F1F18){
            comm = *(uint32_t*) &rxDataEntryBuffer[20];
            //if() == 0x18981F18){
            if(comm != comm_prev){
                comm_prev = comm;
                comCounter = 0;
            }else{
                if(++comCounter == 3){
                    if(comm == 0x18981F18){
                        mComm = 2;
                        rStatus |= 0x01;
                    }
                    if(comm == 0x1818981F){
                        mComm = 1;
                        rStatus |= 0x02;
                    }
                    comCounter = 2;
                    //memcpy(packet, packetDataPointer, 16);
                }
            }
        }
        /* Copy the payload + the status byte to the packet variable */

        RFQueue_nextEntry();
    }
}
