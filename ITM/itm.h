#ifndef ITM_ITM_H_
#define ITM_ITM_H_

#include <ti/devices/DeviceFamily.h>
#include DeviceFamily_constructPath(inc/hw_types.h)
#include DeviceFamily_constructPath(inc/hw_memmap.h)
#include DeviceFamily_constructPath(inc/hw_cpu_itm.h)
#include DeviceFamily_constructPath(inc/hw_cpu_scs.h)
#include DeviceFamily_constructPath(inc/hw_cpu_tpiu.h)
#include DeviceFamily_constructPath(driverlib/ioc.h)

typedef volatile unsigned* ITM_port_t;

void itm_init(void);
void port_wait(ITM_port_t port);
void ITM_put_string(ITM_port_t port, const char *data);

#endif /* ITM_ITM_H_ */
