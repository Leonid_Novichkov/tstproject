#ifndef __BUTTON_H__
#define __BUTTON_H__

#include "stdint.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum _bState{
    bUnknown,
    pressed,
    unpressed
}bState;

typedef enum _mState{
    mUnknown,
    off,
    middle,
    close,
    tryingToClose,
    open,
    tryingToOpen,
    errTime,
    errSwitchTwo,
    errSwitchDir_check,
    errSwitchDir_uncheck,
    errorState = 0x80
}mState;

typedef struct __button{
    bState state;
    bState state_prev;
    uint8_t pinState;
    uint8_t stateCounter;
    uint8_t stateLim;
    uint8_t pinNum;
    void (*actionDown)(void);
    void (*actionUp)(void);
}Button;

#ifdef __cplusplus
}
#endif

#endif
