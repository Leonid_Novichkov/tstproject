/*
 * uDMA_UART_CC1310.h
 */

#ifndef ti_drivers_uart_UDMA_UART_CC1310__include
#define ti_drivers_uart_UDMA_UART_CC1310__include

#include <stdint.h>
#include <stdbool.h>
#include <ti/drivers/dma/UDMACC26XX.h>
#include <ti/drivers/uart/UARTCC26X0.h>


#include <ti/drivers/dpl/DebugP.h>
#include <ti/drivers/dpl/ClockP.h>
#include <ti/drivers/dpl/HwiP.h>
#include <ti/drivers/dpl/SemaphoreP.h>
#include <ti/drivers/dpl/SwiP.h>
#include <ti/drivers/Power.h>
#include <ti/drivers/UART.h>
#include <ti/drivers/pin/PINCC26XX.h>

#include <ti/drivers/power/PowerCC26XX.h>


#include "ti\devices\cc13x0\driverlib\prcm.h"
#include "ti\devices\cc13x0\driverlib\udma.h"
#include "ti\devices\cc13x0\driverlib\uart.h"
#include "ti\devices\cc13x0\driverlib\event.h"

#include <ti/sysbios/knl/Queue.h>
#include <ti/devices/DeviceFamily.h>
#include DeviceFamily_constructPath(inc/hw_memmap.h)
#include DeviceFamily_constructPath(inc/hw_ints.h)
#include DeviceFamily_constructPath(inc/hw_types.h)
#include DeviceFamily_constructPath(driverlib/uart.h)
#include DeviceFamily_constructPath(driverlib/udma.h)
#include DeviceFamily_constructPath(driverlib/sys_ctrl.h)
#include DeviceFamily_constructPath(driverlib/ioc.h)
#include DeviceFamily_constructPath(driverlib/aon_ioc.h)

typedef void (*UART_CC1310_ErrorCallback) (UART_Handle handle, uint32_t error);

/* UART function table pointer */
extern const UART_FxnTable uDMA_UARTCC1310_fxnTable;

typedef struct uartOutData {
    uint8_t *data;
    uint16_t size;
}uartOutData;

typedef struct qRec {
    Queue_Elem elem;
    uartOutData uRec;
}qRec;

typedef struct _uDMA_UARTCC1310_HWAttrs {

    uint32_t        baseAddr; /*! UART Peripheral's base address */
    uint32_t     powerMngrId; /*!< UART Peripheral's power manager ID */
    int             intNum;/*! UART Peripheral's interrupt vector */
    /*! UART Peripheral's interrupt priority */
    uint8_t         intPriority;
    /*!
     *  @brief Swi priority.
     *  The higher the number, the higher the priority.  The minimum
     *  priority is 0 and the maximum is defined by the underlying OS.
     */
    uint32_t        swiPriority;
    /*! Hardware flow control setting */
    uint32_t        flowControl;
    /*! Pointer to an application ring buffer */
    unsigned char  *ringBufPtr;
    /*! Size of ringBufPtr */
    size_t          ringBufSize;
    /*! UART RX pin assignment */
    uint8_t         rxPin;
    /*! UART TX pin assignment */
    uint8_t         txPin;
    /*! UART clear to send (CTS) pin assignment */
    uint8_t         ctsPin;
    /*! UART request to send (RTS) pin assignment */
    uint8_t         rtsPin;
    /*! UART TX interrupt FIFO threshold select */
    UARTCC26X0_FifoThreshold txIntFifoThr;
    /*! UART RX interrupt FIFO threshold select */
    UARTCC26X0_FifoThreshold rxIntFifoThr;
    /*! Application error function to be called on receive errors */
    UART_CC1310_ErrorCallback errorFxn;
} uDMA_UARTCC1310_HWAttrs;

typedef struct uDMA_UART_CC1310_Object {
    /* UART state variable */
    struct {
        bool             opened:1;         /* Has the obj been opened */
        /*
         * Flag to determine when an ISR needs to perform a callback;
         */
        bool             callCallback:1;
        /* Keep track of RX enabled state set by app with UART_control() */
        bool             ctrlRxEnabled:1;
        /* Flag to keep the state of the read Power constraints */
        bool             rxEnabled:1;
        /* Flag to keep the state of the write Power constraints */
        bool             txEnabled:1;
    } state;

    HwiP_Struct          hwi;              /* Hwi object for interrupts */
    SwiP_Struct          swi;              /* Swi for read/write callbacks */

    ClockP_Struct        timeoutClk;       /* Clock object to for timeouts */
    ClockP_Struct        txFifoEmptyClk;   /* UART TX FIFO empty clock */
    ClockP_Struct        queueTimer;
    uint32_t             baudRate;         /* Baud rate for UART */
    UART_LEN             dataLength;       /* Data length for UART */
    UART_STOP            stopBits;         /* Stop bits for UART */
    UART_PAR             parityType;       /* Parity bit type for UART */
    uint32_t             status;           /* RX status */

    /* UART read variables */
    unsigned char       *readBuf;          /* Buffer data pointer */
    size_t               readSize;         /* Desired number of bytes to read */
    UART_Callback        readCallback;     /* Pointer to read callback */
    //bool                 readRetPartial;   /* Return partial RX data if timeout occurs */

    /* UART write variables */
    const unsigned char *writeBuf;         /* Buffer data pointer */
    size_t               writeSize;        /* Desired number of bytes to write*/
    UART_Callback        writeCallback;    /* Pointer to write callback */

    /* PIN driver state object and handle */
    PIN_State            pinState;
    PIN_Handle           hPin;

    /* UDMA driver handle */
    UDMACC26XX_Handle               udmaHandle;                 /*!< UDMA handle */

    //SemaphoreP_Handle   writeSem;
    //SemaphoreP_Handle   readSem;

    Queue_Handle writeQueue;
    qRec *pRec;
    /* For Power management */
    Power_NotifyObj      postNotify;
    bool                 uartPowerConstraint;
    bool isReadyToTransmit;
} uDMA_UART_CC1310_Object, *uDMA_UART_CC1310_Handle;

#define READ_DONE  0x1  /* Mask to trigger Swi on a read complete */
#define WRITE_DONE 0x2  /* Mask to trigger Swi on a write complete */

#endif /* ti_drivers_uart_UDMA_UART_CC1310__include */
