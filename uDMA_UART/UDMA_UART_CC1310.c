/*
 * uDMA_UART.c
 */

#include "UDMA_UART_CC1310.h"
#include <ti/drivers/uart/UARTCC26XX.h>
#include <xdc/std.h>
#include <xdc/runtime/IHeap.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Memory.h>
#include <xdc/runtime/Error.h>

#define mask(x) (1<<(x))
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))
#define UNIT_DIV_ROUNDUP(x,d) ((x + ((d) - 1)) / (d))

extern Error_Block eb;

ALLOCATE_CONTROL_TABLE_ENTRY(dmaUART_TxControlTableEntry, UDMA_CHAN_UART0_TX);
ALLOCATE_CONTROL_TABLE_ENTRY(dmaUART_RxControlTableEntry, UDMA_CHAN_UART0_RX);

/* UARTCC26X0 functions */
static void uDMA_UARTCC1310_close(UART_Handle handle);
int_fast16_t uDMA_UARTCC1310_control(UART_Handle handle, uint_fast16_t cmd, void *arg);
void uDMA_UARTCC1310_init(UART_Handle handle);
UART_Handle uDMA_UARTCC1310_open(UART_Handle handle, UART_Params *params);
int_fast32_t uDMA_UARTCC1310_read(UART_Handle handle, void *buffer, size_t size);
int_fast32_t uDMA_UARTCC1310_readPolling(UART_Handle handle, void *buf, size_t size);
int_fast32_t uDMA_UARTCC1310_readPollingNotImpl(UART_Handle handle, void *buf, size_t size);
void uDMA_UARTCC1310_readCancel(UART_Handle handle);
int_fast32_t uDMA_UARTCC1310_write(UART_Handle handle, const void *buffer, size_t size);
int_fast32_t uDMA_UARTCC1310_writePolling(UART_Handle handle, const void *buf, size_t size);
int_fast32_t uDMA_UARTCC1310_writePollingNotImpl(UART_Handle handle, const void *buf, size_t size);
int_fast32_t uDMA_UARTCC1310_writeToQueue(UART_Handle handle, const void *buffer, size_t size);
void uDMA_UARTCC1310_writeCancel(UART_Handle handle);
void uartClearRxFIFO(UART_Handle handle);
qRec *pRec;

/* Static functions */
static void uDMA_UARTCC1310_hwiIntFxn(uintptr_t arg);
//static void disableRX(UART_Handle handle);
static void enableRX(UART_Handle handle);
static void initHw(UART_Handle handle);
static bool initIO(UART_Handle handle);
static int postNotifyFxn(unsigned int eventType, uintptr_t eventArg, uintptr_t clientArg);
//static void readBlockingTimeout(uintptr_t arg);
//static void readIsr(UART_Handle handle, uint32_t status);
//static void readSemCallback(UART_Handle handle, void *buffer, size_t count);
//static int readTaskBlocking(UART_Handle handle);
//static int readTaskCallback(UART_Handle handle);
//static int ringBufGet(UART_Handle handle, unsigned char *data);
static void startTxFifoEmptyClk(UART_Handle handle, uint32_t numDataInFifo);
static void swiCallback(uintptr_t arg0, uintptr_t arg1);
//static void writeData(UART_Handle handle, bool inISR);
static void writeFinishedDoCallback(UART_Handle handle);
//static void writeSemCallback(UART_Handle handle, void *buffer, size_t count);

static const uint32_t dataLength[] = {
                                       UART_CONFIG_WLEN_5, /* UART_LEN_5 */
                                       UART_CONFIG_WLEN_6, /* UART_LEN_6 */
                                       UART_CONFIG_WLEN_7, /* UART_LEN_7 */
                                       UART_CONFIG_WLEN_8 /* UART_LEN_8 */
};

static const uint32_t stopBits[] = {
                                     UART_CONFIG_STOP_ONE, /* UART_STOP_ONE */
                                     UART_CONFIG_STOP_TWO /* UART_STOP_TWO */
};

static const uint32_t parityType[] = {
                                       UART_CONFIG_PAR_NONE, /* UART_PAR_NONE */
                                       UART_CONFIG_PAR_EVEN, /* UART_PAR_EVEN */
                                       UART_CONFIG_PAR_ODD, /* UART_PAR_ODD */
                                       UART_CONFIG_PAR_ZERO, /* UART_PAR_ZERO */
                                       UART_CONFIG_PAR_ONE /* UART_PAR_ONE */
};

static const uint8_t txFifoBytes[6] = {
                                        4, /* UARTCC26XX_FIFO_THRESHOLD_DEFAULT */
                                        4, /* UARTCC26XX_FIFO_THRESHOLD_1_8     */
                                        8, /* UARTCC26XX_FIFO_THRESHOLD_2_8     */
                                        16, /* UARTCC26XX_FIFO_THRESHOLD_4_8     */
                                        24, /* UARTCC26XX_FIFO_THRESHOLD_6_8     */
                                        28 /* UARTCC26XX_FIFO_THRESHOLD_7_8     */
};

/*
 * Ensure safe setting of the standby disallow constraint.
 */
inline void threadSafeConstraintSet(uint32_t txBufAddr, uDMA_UART_CC1310_Object *object) {
    unsigned int key;

    /* Disable interrupts */
    key = HwiP_disable();

    if(!object->uartPowerConstraint){
        /* Ensure flash is available if TX buffer is in flash. Flash starts with 0x0..*/
        /*  if (((txBufAddr & 0xF0000000) == 0x0) && (txBufAddr)) {
         Power_setConstraint(PowerCC26XX_NEED_FLASH_IN_IDLE);
         Power_setConstraint(PowerCC26XX_DISALLOW_XOSC_HF_SWITCHING);
         }*/
        /* Set constraints to guarantee operation */
        Power_setConstraint(PowerCC26XX_DISALLOW_STANDBY);

        object->uartPowerConstraint = true;
    }

    /* Re-enable interrupts */
    HwiP_restore(key);
}

/*
 * Ensure safe releasing of the standby disallow constraint.
 */
inline void threadSafeConstraintRelease(uint32_t txBufAddr, uDMA_UART_CC1310_Object *object) {
    unsigned int key;

    /* Disable interrupts */
    key = HwiP_disable();

    if(object->uartPowerConstraint){
        /* Release need flash if buffer was in flash. */
        /* if (((txBufAddr & 0xF0000000) == 0x0) && (txBufAddr)) {
         Power_releaseConstraint(PowerCC26XX_DISALLOW_XOSC_HF_SWITCHING);
         Power_releaseConstraint(PowerCC26XX_NEED_FLASH_IN_IDLE);
         }*/
        /* Release standby constraint since operation is done. */
        Power_releaseConstraint(PowerCC26XX_DISALLOW_STANDBY);
        object->uartPowerConstraint = false;
    }

    /* Re-enable interrupts */
    HwiP_restore(key);
}

static void enableRX(UART_Handle handle) {

}

void uDMA_UARTCC1310_init(UART_Handle handle) {

}

/*
 *  ======== startTxFifoEmptyClk ========
 *  Last write to TX FIFO is done, but not shifted out yet. Start a clock
 *  which will trigger when the TX FIFO should be empty.
 */
static void startTxFifoEmptyClk(UART_Handle handle, uint32_t numDataInFifo) {
    uDMA_UART_CC1310_Object *object = handle->object;

    /* Ensure that the clock is stopped so we can set a new timeout */
    ClockP_stop((ClockP_Handle) &(object->txFifoEmptyClk));

    /* No more to write, but data is not shifted out properly yet.
     *   1. Compute appropriate wait time for FIFO to empty out
     *       - 1 bit for start bit
     *       - 5+(object->dataLength) for total data length
     *       - +1 to "map" from stopBits to actual number of bits
     *       - 1000000 so we get 1 us resolution
     *       - 100 (100us) for margin
     */
    unsigned int writeTimeoutUs = (numDataInFifo * (1 + 5 + (object->dataLength) + (object->stopBits + 1)) * 1000000) / object->baudRate + 100;

    /*   2. Configure clock object to trigger when FIFO is empty
     *       - + 1 in case clock module due to tick is less than one ClockP
     *         tick period
     *       - UNIT_DIV_ROUNDUP to avoid fractional part being truncated
     *         during division
     */

    ClockP_setTimeout((ClockP_Handle) &(object->txFifoEmptyClk), (1 + UNIT_DIV_ROUNDUP(writeTimeoutUs, ClockP_tickPeriod)));
    ClockP_start((ClockP_Handle) &(object->txFifoEmptyClk));
}

static void uDMA_UARTCC1310_hwiIntFxn(uintptr_t arg) {
    uDMA_UARTCC1310_HWAttrs const *hwAttrs = ((UART_Handle) arg)->hwAttrs;
    uDMA_UART_CC1310_Object *object = ((UART_Handle) arg)->object;

    uint32_t status = UARTIntStatus(hwAttrs->baseAddr, true);
    UARTIntClear(hwAttrs->baseAddr, status); //Clear interrupts

    if(UDMACC26XX_channelDone(object->udmaHandle, mask(UDMA_CHAN_UART0_TX))){
        UARTDMADisable(hwAttrs->baseAddr, UART_DMA_TX); //Disable UART TX DMA and clear DMA done interrupt.
        UDMACC26XX_clearInterrupt(object->udmaHandle, mask(UDMA_CHAN_UART0_TX));
        startTxFifoEmptyClk((UART_Handle) arg, txFifoBytes[hwAttrs->txIntFifoThr]);
    }

    if(UDMACC26XX_channelDone(object->udmaHandle, mask(UDMA_CHAN_UART0_RX))){
        UARTDMADisable(hwAttrs->baseAddr, UART_DMA_RX); //Disable SPI RX DMA and clear DMA done interrupt.
        UDMACC26XX_clearInterrupt(object->udmaHandle, mask(UDMA_CHAN_UART0_RX));
        SwiP_or(&(object->swi), READ_DONE);
    }

    /* Post SWI to handle remaining clean up and invocation of callback */
    //SwiP_post(&(object->swi));
}

static void initHw(UART_Handle handle) {
    ClockP_FreqHz freq;
    uDMA_UART_CC1310_Object *object = handle->object;
    uDMA_UARTCC1310_HWAttrs const *hwAttrs = handle->hwAttrs;

    /*
     *  Configure frame format and baudrate.  UARTConfigSetExpClk() disables
     *  the UART and does not re-enable it, so call this function first.
     */
    ClockP_getCpuFreq(&freq); // object->dataLength|object->stopBits|object->parityType
    UARTConfigSetExpClk(hwAttrs->baseAddr, freq.lo, object->baudRate, stopBits[object->stopBits] | parityType[object->parityType] | dataLength[object->dataLength]);

    UARTIntClear(hwAttrs->baseAddr, UART_INT_OE | UART_INT_BE | UART_INT_PE | UART_INT_FE | UART_INT_RT | UART_INT_TX | UART_INT_RX | UART_INT_CTS); //Clear all UART interrupts

    UARTFIFOLevelSet(hwAttrs->baseAddr, UART_FIFO_TX1_8, UART_FIFO_RX1_8); //Set TX interrupt FIFO level and RX interrupt FIFO level

    /*if(isFlowControlEnabled(hwAttrs)){ //If Flow Control is enabled, configure hardware flow control
     UARTHwFlowControlEnable(hwAttrs->baseAddr);
     }else{
     UARTHwFlowControlDisable(hwAttrs->baseAddr);
     }*/

    HWREG(hwAttrs->baseAddr + UART_O_LCRH) |= UART_LCRH_FEN; //Enable UART FIFOs
    //HWREG(hwAttrs->baseAddr + UART_O_CTL) |= UART_CTL_UARTEN; //Enable the UART module

    uint32_t ctl_val = UART_CTL_UARTEN | UART_CTL_TXE | UART_CTL_RXE;
    HWREG(UART0_BASE + UART_O_CTL) = ctl_val;

    if(object->state.ctrlRxEnabled){
        /* Enable RX */
        enableRX(handle);
    }
}

static bool initIO(UART_Handle handle) {
    uDMA_UART_CC1310_Object *object = handle->object;
    uDMA_UARTCC1310_HWAttrs const *hwAttrs = handle->hwAttrs;
    PIN_Config uartPinTable[5];
    uint32_t pinCount = 0;

    /* Build local list of pins, allocate through PIN driver and map ports */
    uartPinTable[pinCount++] = hwAttrs->rxPin | PIN_INPUT_EN;
    /*
     *  Make sure UART_TX pin is driven high after calling PIN_open(...) until
     *  we've set the correct peripheral muxing in PINCC26XX_setMux(...).
     *  This is to avoid falling edge glitches when configuring the
     *  UART_TX pin.
     */
    uartPinTable[pinCount++] = hwAttrs->txPin | PIN_INPUT_DIS | PIN_PUSHPULL | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH;

    /* if(isFlowControlEnabled(hwAttrs)){
     uartPinTable[pinCount++] = hwAttrs->ctsPin | PIN_INPUT_EN;
     uartPinTable[pinCount++] = hwAttrs->rtsPin | PIN_INPUT_DIS | PIN_PUSHPULL | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH;//Avoiding glitches on the RTS, see comment for TX pin above.
     }*/

    /* Terminate pin list */
    uartPinTable[pinCount] = PIN_TERMINATE;

    /* Open and assign pins through pin driver */
    object->hPin = PIN_open(&object->pinState, uartPinTable);

    /* Are pins already allocated */
    if(!object->hPin){
        return (false);
    }

    /* Set IO muxing for the UART pins */
    PINCC26XX_setMux(object->hPin, hwAttrs->rxPin, IOC_PORT_MCU_UART0_RX);
    PINCC26XX_setMux(object->hPin, hwAttrs->txPin, IOC_PORT_MCU_UART0_TX);

    /*if(isFlowControlEnabled(hwAttrs)){
     PINCC26XX_setMux(object->hPin, hwAttrs->ctsPin,
     IOC_PORT_MCU_UART0_CTS);
     PINCC26XX_setMux(object->hPin, hwAttrs->rtsPin,
     IOC_PORT_MCU_UART0_RTS);
     }*/
    /* Success */
    return (true);
}

static int postNotifyFxn(unsigned int eventType, uintptr_t eventArg, uintptr_t clientArg) {
    if(eventType == PowerCC26XX_AWAKE_STANDBY){ //Reconfigure the hardware if returning from sleep
        initHw((UART_Handle) clientArg);
    }
    return (Power_NOTIFYDONE);
}

static void swiCallback(uintptr_t arg0, uintptr_t arg1) {
    uDMA_UARTCC1310_HWAttrs const *hwAttrs = ((UART_Handle) arg0)->hwAttrs;
    uDMA_UART_CC1310_Object *object = ((UART_Handle) arg0)->object;
    uint32_t trigger = SwiP_getTrigger();
    //uintptr_t key;
    //size_t count;

    //threadSafeConstraintRelease(0x0000001, object);
    if(trigger & WRITE_DONE){
        /*
         *  Check txEnabled before releasing Power constraint.
         *  UARTCC26X0_writeCancel() could have been called and released the
         *  constraint.
         */
        /*key = HwiP_disable();
         if (object->state.txEnabled) {
         Power_releaseConstraint(PowerCC26XX_DISALLOW_STANDBY);
         object->state.txEnabled = false;
         }
         HwiP_restore(key);*/

        /* Disable TX interrupt */
        UARTIntDisable(hwAttrs->baseAddr, UART_INT_TX);

        /* Disable TX */
        //HWREG(hwAttrs->baseAddr + UART_O_CTL) &= ~(UART_CTL_TXE);
        /* Make callback */
        object->writeCallback((UART_Handle) arg0, (uint8_t*) object->writeBuf, object->writeSize);

        threadSafeConstraintRelease(0x0000001, object);
    }
    if(trigger & READ_DONE){
        UARTIntDisable(hwAttrs->baseAddr, UART_INT_RX);
        object->readCallback((UART_Handle) arg0, (uint8_t*) object->readBuf, object->readSize);
    }
}

uint16_t enqueueCounter;

bool addDataToUartQueue(UART_Handle handle, const void *buffer, size_t size) {
    uDMA_UART_CC1310_Object *object = handle->object;
    //void *qr = malloc(sizeof(qRec));
    void *qr = Memory_alloc(NULL, sizeof(qRec), 0, &eb);
    if(qr != 0){
        qRec *out_r = (qRec*) qr;
        out_r->uRec.data = (uint8_t*) buffer;
        out_r->uRec.size = size;
        Queue_enqueue(object->writeQueue, &(out_r->elem));
        enqueueCounter += 1;
        return true;
    }
    return false;
}

void fromQueueToUart(UART_Handle handle) {
    uDMA_UART_CC1310_Object *object = handle->object;
    if(!Queue_empty(object->writeQueue)){
        if(object->isReadyToTransmit){
            pRec = Queue_dequeue(object->writeQueue);
            object->pRec = pRec;
            uDMA_UARTCC1310_write(handle, object->pRec->uRec.data, object->pRec->uRec.size);
        }
    }
    //ClockP_setTimeout((ClockP_Handle) &(object->queueTimer), 4800);
    //ClockP_start((ClockP_Handle) &(object->queueTimer));
}

int_fast32_t uDMA_UARTCC1310_write(UART_Handle handle, const void *buffer, size_t size) {
    uDMA_UART_CC1310_Object *object = handle->object;
    uDMA_UARTCC1310_HWAttrs const *hwAttrs = handle->hwAttrs;
    tDMAControlTable *dmaControlTableEntry;

    threadSafeConstraintSet((uint32_t) buffer, object);

    /* Setup TX side */
    /* Set pointer to Tx control table entry */
    dmaControlTableEntry = (tDMAControlTable*)&dmaUART_TxControlTableEntry;

    dmaControlTableEntry->ui32Control = UDMA_MODE_BASIC | UDMA_SIZE_8 | UDMA_SRC_INC_8 | UDMA_DST_INC_NONE | UDMA_ARB_16;

    // uint32_t ctl_val = UART_CTL_UARTEN | UART_CTL_TXE;
    // HWREG(UART0_BASE + UART_O_CTL) = ctl_val;

    /*
     * Add an offset for the amount of data transfered.  The offset is
     * calculated by: object->amtDataXferred * (object->frameSize + 1).
     * This accounts for 8 or 16-bit sized transfers.
     */
    dmaControlTableEntry->pvSrcEndAddr = (volatile void*) ((uint32_t) buffer + size - 1);   //
    // (void *)((uint32_t) object->currentTransaction->txBuf +
    // ((uint32_t) object->amtDataXferred * (object->frameSize + 1)) +
    // numberOfBytes - 1);

    dmaControlTableEntry->pvDstEndAddr = (void*) (hwAttrs->baseAddr + UART_O_DR);
    dmaControlTableEntry->ui32Control |= UDMACC26XX_SET_TRANSFER_SIZE((uint16_t ) size);

    uDMAChannelRequest(UDMA0_BASE, UDMA_CHAN_UART0_TX);

    /* Enable the channels */
    UDMACC26XX_channelEnable(object->udmaHandle, mask(UDMA_CHAN_UART0_TX));

    /* Enable the required DMA channels in the UART module to start the transaction */
    UARTDMAEnable(hwAttrs->baseAddr, UART_DMA_TX);
    return 0;
}

int_fast32_t uDMA_UARTCC1310_read(UART_Handle handle, void *buffer, size_t size) {
    uDMA_UART_CC1310_Object *object = handle->object;
    uDMA_UARTCC1310_HWAttrs const *hwAttrs = handle->hwAttrs;
    tDMAControlTable *dmaControlTableEntry;

    //threadSafeConstraintSet(buffer, object);

    /* Setup TX side */
    /* Set pointer to Tx control table entry */
    dmaControlTableEntry = (tDMAControlTable*)&dmaUART_RxControlTableEntry;

    dmaControlTableEntry->ui32Control = UDMA_MODE_BASIC | UDMA_SIZE_8 | UDMA_SRC_INC_NONE | UDMA_DST_INC_8 | UDMA_ARB_4;

    // uint32_t ctl_val = UART_CTL_UARTEN | UART_CTL_TXE;
    // HWREG(UART0_BASE + UART_O_CTL) = ctl_val;

    /*
     * Add an offset for the amount of data transfered.  The offset is
     * calculated by: object->amtDataXferred * (object->frameSize + 1).
     * This accounts for 8 or 16-bit sized transfers.
     */
    dmaControlTableEntry->pvSrcEndAddr = (void*) (hwAttrs->baseAddr + UART_O_DR);   // buffer + size - 1;//
    // (void *)((uint32_t) object->currentTransaction->txBuf +
    // ((uint32_t) object->amtDataXferred * (object->frameSize + 1)) +
    // numberOfBytes - 1);

    dmaControlTableEntry->pvDstEndAddr = (void*) ((uint32_t) buffer + (uint32_t) size - 1);
    dmaControlTableEntry->ui32Control |= UDMACC26XX_SET_TRANSFER_SIZE((uint16_t ) size);

    uDMAChannelRequest(UDMA0_BASE, UDMA_CHAN_UART0_RX);

    uartClearRxFIFO(handle);

    /* Enable the channels */
    UDMACC26XX_channelEnable(object->udmaHandle, mask(UDMA_CHAN_UART0_RX));

    /* Enable the required DMA channels in the UART module to start the transaction */
    UARTDMAEnable(hwAttrs->baseAddr, UART_DMA_RX);
    return 0;
}

UART_Handle uDMA_UARTCC1310_open(UART_Handle handle, UART_Params *params) {
    uintptr_t key;
    uDMA_UART_CC1310_Object *object = handle->object;
    uDMA_UARTCC1310_HWAttrs const *hwAttrs = handle->hwAttrs;
    /* Use union to save on stack allocation */
    union {
        HwiP_Params hwiParams;
        ClockP_Params clkParams;
        SwiP_Params swiParams;
        SemaphoreP_Params semParams;
    } paramsUnion;

    /* Check for callback when in UART_MODE_CALLBACK */
    DebugP_assert((params->readMode != UART_MODE_CALLBACK) || (params->readCallback != NULL));DebugP_assert((params->writeMode != UART_MODE_CALLBACK) || (params->writeCallback != NULL));

    key = HwiP_disable();

    if(object->state.opened == true){
        HwiP_restore(key);

        DebugP_log1("UART:(%p) already in use.", hwAttrs->baseAddr);
        return (NULL);
    }
    object->state.opened = true;

    HwiP_restore(key);

    object->readCallback = params->readCallback;
    object->writeCallback = params->writeCallback;
    object->baudRate = params->baudRate;
    object->stopBits = params->stopBits;
    object->dataLength = params->dataLength;
    object->parityType = params->parityType;

    /* Set UART transaction variables to defaults. */
    object->writeBuf = NULL;
    object->readBuf = NULL;
    //object->writeCount = 0;
    // object->readCount = 0;
    object->writeSize = 0;
    object->readSize = 0;
    object->status = 0;
    //object->readRetPartial = false;
    object->state.rxEnabled = false;
    object->state.ctrlRxEnabled = false;
    object->state.txEnabled = false;
    //object->state.drainByISR = false;

    /* Register power dependency - i.e. power up and enable clock for UART. */
    Power_setDependency(PowerCC26XX_PERIPH_UART0);

    UARTDisable(hwAttrs->baseAddr);

    /* Configure IOs, make sure it was successful */
    if(!initIO(handle)){
        /* Another driver or application already using these pins. */
        DebugP_log0("Could not allocate pins, already in use.");
        /* Release power dependency */
        Power_releaseDependency(PowerCC26XX_PERIPH_UART0);
        /* Mark the module as available */
        object->state.opened = false;
        return (NULL);
    }

    /* Initialize the UART hardware module */
    initHw(handle);

    /* Register notification function */
    Power_registerNotify(&object->postNotify, PowerCC26XX_AWAKE_STANDBY, postNotifyFxn, (uintptr_t) handle);

    /* Create Hwi object for this UART peripheral. */
    HwiP_Params_init(&(paramsUnion.hwiParams));
    paramsUnion.hwiParams.arg = (uintptr_t) handle;
    paramsUnion.hwiParams.priority = hwAttrs->intPriority;
    HwiP_construct(&(object->hwi), hwAttrs->intNum, uDMA_UARTCC1310_hwiIntFxn, &(paramsUnion.hwiParams));

    SwiP_Params_init(&(paramsUnion.swiParams));
    paramsUnion.swiParams.arg0 = (uintptr_t) handle;
    paramsUnion.swiParams.priority = hwAttrs->swiPriority;
    SwiP_construct(&(object->swi), swiCallback, &(paramsUnion.swiParams));

    /* Create clock object to be used for write FIFO empty callback */
    ClockP_Params_init(&paramsUnion.clkParams);
    paramsUnion.clkParams.period = 0;
    paramsUnion.clkParams.startFlag = false;
    paramsUnion.clkParams.arg = (uintptr_t) handle;
    ClockP_construct(&(object->txFifoEmptyClk), (ClockP_Fxn) &writeFinishedDoCallback, 10, &(paramsUnion.clkParams));  // TODO: timeout = 10?

    object->writeQueue = Queue_create(NULL, NULL);

    /* If read mode is blocking create a semaphore and set callback. */
    // if (object->state.readMode == UART_MODE_BLOCKING) {
    /* Timeout clock for reads */
    //ClockP_construct(&(object->timeoutClk), (ClockP_Fxn) & readBlockingTimeout,
    //       0 /* timeout */, &(paramsUnion.clkParams));
    // SemaphoreP_constructBinary(&(object->readSem), 0);
    // object->readCallback = &readSemCallback;
//}
    /* If write mode is blocking create a semaphore and set callback. */
    /*if (object->state.writeMode == UART_MODE_BLOCKING){
     SemaphoreP_constructBinary(&(object->writeSem), 0);
     object->writeCallback = &writeSemCallback;
     }*/

    /* Declare the dependency on the UDMA driver */
    object->udmaHandle = UDMACC26XX_open();
    //UDMACC26XX_initHw(object->udmaHandle);
    //UDMACC26XX_channelEnable(object->udmaHandle, UDMA_CHAN_UART0_RX);
    // UDMACC26XX_channelEnable(object->udmaHandle, UDMA_CHAN_UART0_TX);
    uDMAChannelAttributeDisable(UDMA0_BASE, UDMA_CHAN_UART0_TX, UDMA_ATTR_ALL);
    uDMAChannelAttributeEnable(UDMA0_BASE, UDMA_CHAN_UART0_TX, UDMA_ATTR_USEBURST);   // |UDMA_ATTR_REQMASK|

    uDMAChannelAttributeDisable(UDMA0_BASE, UDMA_CHAN_UART0_RX, UDMA_ATTR_ALL);
    uDMAChannelAttributeEnable(UDMA0_BASE, UDMA_CHAN_UART0_RX, UDMA_ATTR_USEBURST);   // |UDMA_ATTR_REQMASK|
    //EventRegister(EVENT_O_UDMACH2BSEL, EVENT_UDMACH2BSEL_EV_UART0_TX_DMABREQ);
    //UARTDMAEnable(UART0_BASE, UART_DMA_TX);

    uDMAChannelControlSet( UDMA0_BASE, UDMA_CHAN_UART0_TX | UDMA_PRI_SELECT, UDMA_SIZE_8 | UDMA_SRC_INC_8 | UDMA_DST_INC_NONE | UDMA_ARB_32);

    uDMAChannelControlSet( UDMA0_BASE, UDMA_CHAN_UART0_RX | UDMA_PRI_SELECT, UDMA_SIZE_8 | UDMA_DST_INC_NONE | UDMA_SRC_INC_8 | UDMA_ARB_32);
    DebugP_log1("UART:(%p) opened", hwAttrs->baseAddr);/* UART opened successfully */

    /* Create clock object to be used for write FIFO empty callback */
    /*ClockP_Params_init(&paramsUnion.clkParams);
     paramsUnion.clkParams.period = 0;
     paramsUnion.clkParams.startFlag = false;
     paramsUnion.clkParams.arg = (uintptr_t) handle;
     ClockP_construct(&(object->queueTimer), (ClockP_Fxn) &fromQueueToUart, 10, &(paramsUnion.clkParams));
     ClockP_setTimeout((ClockP_Handle) &(object->queueTimer), 24000);
     ClockP_start((ClockP_Handle) &(object->queueTimer));*/
    object->isReadyToTransmit = true;
    /* Return the handle */
    return (handle);
}

/*
 *  ======== UARTCC26X0_close ========
 */
static void uDMA_UARTCC1310_close(UART_Handle handle) {
    uDMA_UART_CC1310_Object *object = handle->object;
    uDMA_UARTCC1310_HWAttrs const *hwAttrs = handle->hwAttrs;

    /* Deallocate pins */
    PIN_close(object->hPin);

    /* Disable UART and interrupts. */
    UARTIntDisable(hwAttrs->baseAddr,
    UART_INT_TX | UART_INT_RX | UART_INT_RT | UART_INT_OE | UART_INT_BE | UART_INT_PE | UART_INT_FE | UART_INT_CTS);

    /* Set to false to allow UARTCC26X0_readCancel() to release constraint */
    object->state.ctrlRxEnabled = false;

    object->state.opened = false;

    /* Cancel any possible ongoing reads/writes */
    uDMA_UARTCC1310_writeCancel(handle);
//UARTCC26X0_readCancel(handle);
    /*
     *  Disable the UART.  Do not call driverlib function
     *  UARTDisable() since it polls for BUSY bit to clear
     *  before disabling the UART FIFO and module.
     */
    /* Disable UART FIFO */
    HWREG(hwAttrs->baseAddr + UART_O_LCRH) &= ~(UART_LCRH_FEN);
    /* Disable UART module */
    HWREG(hwAttrs->baseAddr + UART_O_CTL) &= ~(UART_CTL_UARTEN | UART_CTL_TXE | UART_CTL_RXE);

    /* Release the uDMA dependency and potentially power down uDMA. */
    UDMACC26XX_close(object->udmaHandle);

    HwiP_destruct(&(object->hwi));
    /* if (object->state.writeMode == UART_MODE_BLOCKING) {
     SemaphoreP_destruct(&(object->writeSem));
     }*/
    /*if (object->state.readMode == UART_MODE_BLOCKING) {
     SemaphoreP_destruct(&(object->readSem));
     ClockP_destruct(&(object->timeoutClk));
     }*/
    SwiP_destruct(&(object->swi));
    ClockP_destruct(&(object->txFifoEmptyClk));

    /* Unregister power notification objects */
    Power_unregisterNotify(&object->postNotify);

    /* Release power dependency - i.e. potentially power down serial domain. */
    Power_releaseDependency(PowerCC26XX_PERIPH_UART0);

    DebugP_log1("UART:(%p) closed", hwAttrs->baseAddr);
}

/*
 *  ======== UARTCC26X0_writeCancel ========
 */
void uDMA_UARTCC1310_writeCancel(UART_Handle handle) {
    uintptr_t key;
    uDMA_UART_CC1310_Object *object = handle->object;
    uDMA_UARTCC1310_HWAttrs const *hwAttrs = handle->hwAttrs;

    key = HwiP_disable();

    /* Return if there is no write. */
    if(!object->state.txEnabled){
        HwiP_restore(key);
        return;
    }

    UDMACC26XX_channelDisable(object->udmaHandle, (1 << UDMA_CHAN_UART0_TX));

    HWREG(hwAttrs->baseAddr + UART_O_CTL) &= ~UART_CTL_TXE;

    UARTIntDisable(hwAttrs->baseAddr, UART_INT_TX);
    UARTIntClear(hwAttrs->baseAddr, UART_INT_TX);

    /* Release constraint since transaction is done */
    Power_releaseConstraint(PowerCC26XX_DISALLOW_STANDBY);
    object->state.txEnabled = false;

    /* Disable TX */
    //HWREG(hwAttrs->baseAddr + UART_O_CTL) &= ~(UART_CTL_TXE);
    HwiP_restore(key);

//object->writeCallback(handle, (void *)object->writeBuf, object->writeSize - object->writeCount);

    DebugP_log2("UART:(%p) Write canceled, %d bytes written", hwAttrs->baseAddr,
            object->writeSize - object->writeCount);
}

void uDMA_UARTCC1310_readCancel(UART_Handle handle) {
    uintptr_t key;
    uDMA_UART_CC1310_Object *object = handle->object;
    uDMA_UARTCC1310_HWAttrs const *hwAttrs = handle->hwAttrs;

    key = HwiP_disable();

    if(!object->state.txEnabled){
        HwiP_restore(key);
        return;
    }

    UDMACC26XX_channelDisable(object->udmaHandle, (1 << UDMA_CHAN_UART0_RX));
    HWREG(hwAttrs->baseAddr + UART_O_CTL) &= ~UART_CTL_RXE;
    UARTIntDisable(hwAttrs->baseAddr, UART_INT_RX);
    UARTIntClear(hwAttrs->baseAddr, UART_INT_RX);
    uartClearRxFIFO(handle);
    Power_releaseConstraint(PowerCC26XX_DISALLOW_STANDBY);
    object->state.rxEnabled = false;
    HwiP_restore(key);
}

void uartClearRxFIFO(UART_Handle handle) {
    uDMA_UARTCC1310_HWAttrs const *hwAttrs = handle->hwAttrs;
    while(((int32_t) UARTCharGetNonBlocking(hwAttrs->baseAddr)) != -1);
}

int_fast32_t uDMA_UARTCC1310_writeToQueue(UART_Handle handle, const void *buffer, size_t size) {
    addDataToUartQueue(handle, buffer, size);
    return 0;
}

/*
 *  ======== writeFinishedDoCallback ========
 *  This function is called when the txFifoEmptyClk times out. The TX FIFO
 *  should now be empty and all bytes have been transmitted. The TX will be
 *  turned off, TX interrupt disabled, and standby allowed again.
 */
static void writeFinishedDoCallback(UART_Handle handle) {
    uDMA_UART_CC1310_Object *object = handle->object;
    uDMA_UARTCC1310_HWAttrs const *hwAttrs = handle->hwAttrs;

    /*
     *  Function verifies that the FIFO is empty via BUSY flag
     *  If not yet ready start the periodic timer and wait another period
     */
    if(UARTBusy(hwAttrs->baseAddr)){
        /*
         *  The UART is still busy.
         *  Wait 500 us before checking again or 1 tick period if the
         *  ClockP_tickPeriod is larger than 500 us.
         */
        ClockP_setTimeout((ClockP_Handle) &(object->txFifoEmptyClk), MAX((500 / ClockP_tickPeriod), 1));
        ClockP_start((ClockP_Handle) &(object->txFifoEmptyClk));
        return;
    }

    /* Post the Swi that will make the callback */
    if(enqueueCounter > 0)
        enqueueCounter -= 1;
    object->isReadyToTransmit = true;
    SwiP_or(&(object->swi), WRITE_DONE);
}

/* UART function table for UARTCC26X0 implementation */
const UART_FxnTable uDMA_UARTCC1310_fxnTable = {
                                                 uDMA_UARTCC1310_close,
                                                 NULL, //uDMA_UARTCC1310_control,
                                                 uDMA_UARTCC1310_init,
                                                 uDMA_UARTCC1310_open,
                                                 uDMA_UARTCC1310_read,
                                                 NULL, //uDMA_UARTCC1310_readPollingNotImpl,
                                                 uDMA_UARTCC1310_readCancel,
                                                 uDMA_UARTCC1310_write,
                                                 uDMA_UARTCC1310_writeToQueue,
                                                 uDMA_UARTCC1310_writeCancel };
