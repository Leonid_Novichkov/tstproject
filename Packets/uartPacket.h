#ifndef __UARTPACKET_H__
#define __UARTPACKET_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"

#pragma pack(push)
#pragma pack(1)
typedef struct _uartPACOutData{
 uint16_t header;
 uint8_t motorStatus;
 uint8_t butStatus;
 uint8_t radioStatus;
 uint16_t adc0;
 uint16_t adc1;
 uint16_t adc2;
 uint16_t adc3;
 uint16_t adc4;
 uint16_t adcMin;
 uint16_t adcMax;
 uint16_t crc16;
}uartPacOutData;
#pragma pack(pop)

typedef struct _uartInData{
 uint16_t header;
 uint16_t tstCounter;
 uint8_t command;
 uint16_t crc16;
}uartInData;


#ifdef __cplusplus
}
#endif

#endif

