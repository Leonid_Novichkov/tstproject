#include <Packets/CRC16.h>

const uint16_t poly = 4129;
//extern unsigned short table[256];
uint16_t table[256];
uint16_t initialValue = 0;

uint16_t ComputeChecksum(uint8_t *data, uint16_t length) {
    uint16_t crc = initialValue;
    uint16_t i;
    for(i = 0; i < length; ++i){
        crc = (uint16_t) ((crc << 8) ^ table[((crc >> 8) ^ (0xff & data[i]))]);
    }
    return crc;
}

void CRC16_CCITT() {
    uint16_t temp, a;
    uint16_t i, j;
    for(i = 0; i < 256; ++i){
        temp = 0;
        a = (uint16_t) (i << 8);
        for(j = 0; j < 8; ++j){
            if(((temp ^ a) & 0x8000) != 0){
                temp = (uint16_t) ((temp << 1) ^ poly);
            }else{
                temp <<= 1;
            }
            a <<= 1;
        }
        table[i] = temp;
    }
}
//#pragma section("L1_code")
uint16_t ADC8_CALC(uint8_t *data_buf, uint16_t count) {
    uint16_t sum = *data_buf++;
    uint16_t i;

    for(i = 1; i < count; ++i){
        sum += *data_buf++;
        if((sum & 0xFF00) != 0){
            sum = sum & 0x00FF;
            sum += 1;
        }
    }
    if((sum & 0xFF00) != 0){
        sum = sum & 0x00FF;
        sum += 1;
    }
    sum = sum & 0x00FF;
    return sum;
}
//#pragma section("L1_code")
uint16_t XOR8_CALC(uint8_t *data_buf, uint16_t count) {
    uint16_t sum_ = *data_buf++;
    uint16_t i;
    for(i = 1; i < count; ++i){
        sum_ = sum_ ^ *data_buf++;
    }
    return sum_;
}

